# Nix configuration

## OSX

```sh
bash <(curl -L https://nixos.org/nix/install) --daemon
nix --extra-experimental-features 'flakes nix-command' shell nixpkgs#git nixpkgs#just
# For brew (optional)
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
