{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    nixpkgs-darwin.url = "github:NixOS/nixpkgs/nixpkgs-23.11-darwin";
    nix-darwin = {
      url = "github:lnl7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      # url = "github:nix-community/home-manager/";
      url = "github:nix-community/home-manager/release-23.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    kmonad = {
      url = "git+https://github.com/kmonad/kmonad?submodules=1&dir=nix";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    nixvim = {
      url = "github:nix-community/nixvim/nixos-23.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nvim-plugin-obsidian = {
      url = "github:epwalsh/obsidian.nvim";
      flake = false;
    };
    nvim-plugin-helm_ls = {
      url = "github:towolf/vim-helm";
      flake = false;
    };
    nvim-plugin-glow = {
      url = "github:ellisonleao/glow.nvim";
      flake = false;
    };
  };

  outputs = {
    nixpkgs,
    agenix,
    nixpkgs-unstable,
    home-manager,
    nixos-hardware,
    nix-darwin,
    nixvim,
    ...
  } @ inputs: {
    nixosConfigurations = {
      "linuxtest" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          {_module.args = {unstablePkgs = inputs.nixpkgs-unstable.legacyPackages.x86_64-linux;};}
          agenix.nixosModules.default
          ./hosts/common/nixos.nix
          ./hosts/nixos/linuxtest
          nixos-hardware.nixosModules.tuxedo-infinitybook-pro14-gen7
          home-manager.nixosModules.home-manager
          {
            home-manager = {
              extraSpecialArgs = {inherit inputs;};
            };
            home-manager.users.pbl.imports = [
              agenix.homeManagerModules.default
              ./home-manager/pbl-nixos.nix
              nixvim.homeManagerModules.nixvim
            ];
          }
        ];
      };
    };

    darwinConfigurations = {
      "pbl-mbp183" = nix-darwin.lib.darwinSystem {
        system = "aarch64-darwin";
        modules = [
          {_module.args = {unstablePkgs = inputs.nixpkgs-unstable.legacyPackages.aarch64-darwin;};}
          agenix.nixosModules.default
          ./hosts/darwin/pbl-mbp183/configuration.nix
          home-manager.darwinModules.home-manager
          {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              extraSpecialArgs = {inherit inputs;};
              users.pbl = import ./home-manager/pbl-darwin.nix;
            };
          }
        ];
      };
      "darwintest" = nix-darwin.lib.darwinSystem {
        system = "aarch64-darwin";
        modules = [
          {_module.args = {unstablePkgs = inputs.nixpkgs-unstable.legacyPackages.aarch64-darwin;};}
          agenix.nixosModules.default
          ./hosts/common/darwin.nix
          ./hosts/darwin/darwintest
          home-manager.darwinModules.home-manager
          {
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              extraSpecialArgs = inputs;
              users.pbl = import ./home-manager/pbl-darwin.nix;
            };
          }
        ];
      };
    };
    formatter.aarch64-darwin = nixpkgs.legacyPackages.aarch64-darwin.alejandra;
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.alejandra;
  };
}
