{pkgs, ...}: {
  home = {packages = with pkgs; [joplin just lua tldr];};
  imports = [
    ./neovim
    ./firefox.nix
    ./zellij.nix
  ];

  nixpkgs.config.allowUnfree = true;

  programs = {
    bat = {
      enable = true;
      config = {theme = "DarkNeon";};
    };
    direnv = {
      enable = true;
      enableZshIntegration = true; # see note on other shells below
      nix-direnv.enable = true;
    };
    eza = {
      enable = true;
      enableAliases = true;
      git = true;
      icons = true;
    };
    lf = {
      enable = true;
      commands = {
        editor-open = ''$$EDITOR $f'';
      };
      extraConfig = let
        previewer = pkgs.writeShellScriptBin "pv.sh" ''
          file=$1
          w=$2
          h=$3
          x=$4
          y=$5
          if [[ "$( ${pkgs.file}/bin/file -Lb --mime-type "$file")" =~ ^image ]]; then
              ${pkgs.kitty}/bin/kitty +kitten icat --silent --stdin no --transfer-mode file --place "''${w}x''${h}@''${x}x''${y}" "$file" < /dev/null > /dev/tty
              exit 1
          fi
          ${pkgs.pistol}/bin/pistol "$file"
        '';
        cleaner = pkgs.writeShellScriptBin "clean.sh" ''
          ${pkgs.kitty}/bin/kitty +kitten icat --clear --stdin no --silent --transfer-mode file < /dev/null > /dev/tty
        '';
      in ''
        set cleaner ${cleaner}/bin/clean.sh
        set previewer ${previewer}/bin/pv.sh
      '';
    };
    mpv = {
      enable = true;
      scripts = with pkgs.mpvScripts; [
        thumbnail
        sponsorblock
        webtorrent-mpv-hook
      ];
    };
    fzf = {
      enable = true;
      enableZshIntegration = true;
    };
    git = {
      enable = true;
      userName = "Pablo Gómez";
      userEmail = "pablogomez@pablogomez.com";
      signing = {key = "E33576E61E060DAD";};
      aliases = {
        aliases = "!git config --list | grep 'alias\\.' | sed 's/alias\\.\\([^=]*\\)=\\(.*\\)/\\1\\ 	 => \\2/' | sort";
        a = "add";
        b = "branch";
        c = "commit";
        ci = "commit";
        co = "checkout";
        cob = "checkout -b";
        l = "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
        pl = "pull";
        s = "status --short";
      };
      extraConfig = {
        init = {defaultBranch = "main";};
        core = {excludefile = "$HOME/.config/git/gitignore";};
        push = {autoSetupRemote = true;};
      };
    };
    home-manager.enable = true;
    kitty = {
      enable = true;
      shellIntegration.enableZshIntegration = true;
      theme = "Tokyo Night";
      settings = {
        confirm_os_window_close = 0;
      };
    };
    qutebrowser = {
      enable = true;
      settings = {
        auto_save.session = true;
        colors.webpage.darkmode.enabled = true;
        tabs.position = "bottom";
        tabs.show = "always"; # always, switching, never
        statusbar.show = "in-mode";
      };
      extraConfig = "
config.bind('<Ctrl+/>', 'hint links spawn --detach mpv {hint-url}')
      ";
      greasemonkey = [
        (pkgs.fetchurl {
          url = "https://raw.githubusercontent.com/afreakk/greasemonkeyscripts/master/youtube_adblock.js";
          sha256 = "sha256-m8hplisKCiPnTd6aERTmgeGh2DcqOq6wfG18q90kwbA=";
        })
        (pkgs.fetchurl {
          url = "https://raw.githubusercontent.com/afreakk/greasemonkeyscripts/master/youtube_sponsorblock.js";
          sha256 = "sha256-e3QgDPa3AOpPyzwvVjPQyEsSUC9goisjBUDMxLwg8ZE=";
        })
      ];
    };

    ripgrep.enable = true;
    zathura = {
      enable = true;
      options = {
        default-bg = "#1a1b26";
        default-fg = "#9aa5ce";
        recolor = "true";
        recolor-darkcolor = "#9aa5ce";
        recolor-lightcolor = "#414868";
      };
    };
  };

  home.file.".local/bin/bookmarks" = {
    executable = true;
    text = ''
      #!/usr/bin/env bash
      xdg-open "$(grep -v '^#' ~/.local/share/bookmarks | wofi --show dmenu | awk '{print $1}')"
    '';
  };

  home.sessionVariables = {
    AWS_SHARED_CREDENTIALS_FILE = "$HOME/.config/aws/credentials";
    AWS_CONFIG_FILE = "$HOME/.config/aws/config";
    _ZL_DATA = "~/.cache/.zlua";
  };
}
