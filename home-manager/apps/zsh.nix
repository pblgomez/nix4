{
  config,
  pkgs,
  ...
}: {
  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    enableAutosuggestions = true;
    enableCompletion = false; # It slows down as is alredy in per-user
    syntaxHighlighting.enable = true;
    history.path = "${config.xdg.dataHome}/zsh/history";
    shellAliases = {
      cat = "bat";

      g = "git";
      gde = "git checkout $(git symbolic-ref refs/remotes/origin/HEAD | awk -F 'origin/' '{print $2}')";
      gitcleanup = "git branch | grep -v $(git symbolic-ref refs/remotes/origin/HEAD | awk -F 'origin/' '{print $2}') | xargs git branch -D && git remote prune origin";

      k = "kubecolor";
      kg = "k get";
      kd = "k describe";
      kns = "${pkgs.kubeswitch}/bin/switch ns";

      update = "sudo nixos-rebuild switch --flake .# --impure";

      v = "nvim";
    };
    shellGlobalAliases = {
      G = "| grep";
      neat = "-o yaml | kubectl neat";
    };
    zplug = {
      enable = true;
      zplugHome = "${config.xdg.dataHome}/zplug";
      plugins = [
        {name = "skywind3000/z.lua";}
        {
          name = "plugins/git-auto-fetch";
          tags = ["from:oh-my-zsh"];
        }
      ];
    };
    initExtra = ''
      bindkey -v  # Use vi mode

      command -v Hyprland >/dev/null && [ "$(tty)" = "/dev/tty1" ] && [ -z "$DISPLAY" ] && exec Hyprland

      setopt correct                                                                        # Auto correct mistakes
      setopt nocaseglob                                                                     # Ignore case
      setopt HIST_IGNORE_SPACE                                                              # Ignore commands starting with space
      zstyle -e ':completion:*' special-dirs '[[ $PREFIX = (../)#(|.|..) ]] && reply=(..)'  # Complete special dir not always ../
      export PATH="$HOME/.local/bin:$PATH"

      zstyle ':completion:*' menu select

      command -v kubecolor >/dev/null 2>&1 && alias kubectl="kubecolor" && compdef kubecolor=kubectl
      alias k='kubecolor'

      export PATH="$PATH:$HOME/.krew/bin"

      command -v direnv >/dev/null && eval "$(direnv hook zsh)"

      # Zellij
      # command -v zellij >/dev/null && export ZELLIJ_AUTO_ATTACH="true"
      # if [ -n "$DISPLAY" ] && [ -z "$ZELLIJ" ]; then
      #   if [[ "$ZELLIJ_AUTO_ATTACH" == "true" ]]; then
      #     zellij attach -c
      #   else
      #     zellij
      #   fi
      #   if [[ "$ZELLIJ_AUTO_EXIT" == "true" ]]; then
      #     exit
      #   fi
      # fi
    '';
  };

  home.sessionVariables = {
    EDITOR = "nvim";
  };
}
