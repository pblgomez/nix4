{pkgs, ...}: let
  stdenv = pkgs.stdenv;
in {
  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball
      "https://github.com/nix-community/NUR/archive/master.tar.gz") {
      inherit pkgs;
    };
  };

  programs = {
    firefox = {
      enable =
        if stdenv.isDarwin
        then false
        else true;
      package = pkgs.wrapFirefox pkgs.firefox-unwrapped {
        extraPrefs = ''

          // Dark theme
          lockPref("browser.theme.toolbar-theme", 0);
          lockPref("browser.theme.content-theme", 0);
          lockPref("extensions.activeThemeID", "firefox-compact-dark@mozilla.org");
          lockPref("layout.css.prefers-color-scheme.content-override", 0);

          // Tabs
          lockPref("browser.tabs.firefox-view", false);
          lockPref("browser.ctrlTab.sortByRecentlyUsed", true);

          // Remove List All Tabs
          lockPref("browser.tabs.tabmanager.enabled", false);

          // Show more ssl cert infos
          lockPref("security.identityblock.show_extended_validation", true);

          // Enable dark dev tools
          lockPref("devtools.theme","dark");

        '';
        extraPolicies = {
          Containers = {
            Default = [
              {
                "name" = "Personal";
                "icon" = "fingerprint";
                "color" = "purple";
              }
              {
                "name" = "Work Prod";
                "icon" = "briefcase";
                "color" = "red";
              }
              {
                "name" = "Work Dev";
                "icon" = "briefcase";
                "color" = "green";
              }
              {
                "name" = "Shopping";
                "icon" = "cart";
                "color" = "orange";
              }
              {
                "name" = "Banking";
                "icon" = "dollar";
                "color" = "pink";
              }
              {
                "name" = "Social";
                "icon" = "fence";
                "color" = "blue";
              }
            ];
          };
          DisablePocket = true;
          DisableFirefoxAccounts = true;
          DisableStudies = true;
          DisableTelemetry = true;
          Extensions = {
            Install = [
              "https://addons.mozilla.org/firefox/downloads/latest/bitwarden-password-manager/addon-12533945-latest.xpi"
              "https://addons.mozilla.org/firefox/downloads/latest/multi-account-containers/addon-4757633-latest.xpi"
              "https://addons.mozilla.org/firefox/downloads/latest/tridactyl-vim/addon-4208422-latest.xpi"
              "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi"
            ];
          };
          FirefoxHome = {
            TopSites = false;
            SponsoredTopSites = false;
          };
          Homepage.StartPage = "previous-session";
          ManagedBookmarks = [
            {
              "name" = "NixOS";
              "children" = [
                {
                  "name" = "Firefox NixOS wrapper";
                  "url" = "https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/networking/browsers/firefox/wrapper.nix";
                }
                {
                  "name" = "Firefox policies";
                  "url" = "https://github.com/mozilla/policy-templates/blob/master/linux/policies.json";
                }
              ];
            }
          ];
          NoDefaultBookmarks = true;
          PasswordManagerEnabled = false;
          UserMessaging = {
            ExtensionRecommendations = false;
            SkipOnboarding = true;
          };
        };
      };
    };
  };
  programs.firefox.profiles = {
    pbl = {
      id = 0;
      name = "pbl";
      search.default = "DuckDuckGo";
      search.force = true;
    };
  };
}
