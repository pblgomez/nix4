{pkgs, ...}: {
  programs.nixvim = {
    plugins = {
      markdown-preview.enable = true;
      markdown-preview.browser = "${pkgs.qutebrowser}/bin/qutebrowser";
    };
    keymaps = [
      {
        action = ":MarkdownPreviewToggle<CR>";
        key = "<leader>m";
      }
    ];
  };
}
