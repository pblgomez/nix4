{
  programs.nixvim = {
    plugins = {
      none-ls = {
        enable = true;
        sources = {
          code_actions = {
            shellcheck.enable = true;
            statix.enable = true;
          };
          diagnostics = {
            deadnix.enable = true;
            flake8.enable = true;
            gitlint.enable = true;
            hadolint.enable = true;
            # markdownlint.enable = true; # Not yet in 23.11
            shellcheck.enable = true;
          };
          formatting = {
            alejandra.enable = true;
            shfmt = {
              enable = true;
              withArgs =
                /*
                lua
                */
                ''
                  ({ extra_args = {'-i', '2'}})
                '';
            };
            black.enable = true;
            isort.enable = true;
          };
        };
      };
    };
    keymaps = [
      {
        action = ":lua vim.lsp.buf.code_action()<CR>";
        key = "<leader>ca";
      }
    ];
  };
}
