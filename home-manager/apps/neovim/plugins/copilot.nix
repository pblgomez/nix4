{
  programs.nixvim = {
    plugins = {
      copilot-cmp.enable = true;
    };
    globals.copilot_assume_mapped = true;
  };
}
