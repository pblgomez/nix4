{
  pkgs,
  inputs,
  ...
}: {
  imports = [
    ./keymaps.nix
    ./plugins/copilot.nix
    ./plugins/markdown-preview.nix
    ./plugins/obsidian.nix
    ./plugins/none-ls.nix
    ./plugins/telescope.nix
  ];

  programs.nixvim = {
    enable = true;
    viAlias = true;
    vimAlias = true;

    autoCmd = [
      {
        desc = "Remove ExtraWhitespaces ':%s/\s\+$//e'";
        event = "BufWritePre";
        command = ":%s/\\s\\+$//e";
      }
      {
        desc = "Format files before write lua vim.lsp.buf.format({ async = false })";
        event = "BufWritePre";
        pattern = ["*.lua" "*.py" "*.sh"];
        command = "lua vim.lsp.buf.format({ async = false })";
      }
    ];
    colorschemes.tokyonight.enable = true;
    globals.mapleader = " ";

    highlight = {
      ExtraWhitespace.bg = "red";
      TODO = {
        fg = "blue";
        bg = "yellow";
      };
    };
    match = {
      ExtraWhitespace = "\\s\\+$";
      TODO = "TODO";
    };

    options = {
      number = true;
      relativenumber = true;
      expandtab = true; # convert tabs to spaces
      shiftwidth = 2; # the number of spaces inserted for each indentation
      conceallevel = 1; # Hide * markup
      colorcolumn = "80,160";
      cursorline = true; # Highlight current line
      ignorecase = true; # Ignore case when searching
      inccommand = "split"; # Show live preview when substituting
      scrolloff = 20; # Lines of context
      smartcase = true; # Ignore case when searching lowercase
      smartindent = true; # Insert indents automatically
      softtabstop = 2; # Number of spaces that a <Tab> in the file counts for
      splitbelow = true; # Vertical split
      splitright = true; # Vertical split to the right
      swapfile = false; # Disable swapfile
      undofile = true; # Enable undofile
      cmdheight = 0; # Disable command line
      virtualedit = "block"; # Allow cursor to move where there is no text
      wrap = false;
    };
    plugins = {
      comment-nvim.enable = true;

      fugitive.enable = true;

      gitblame = {
        enable = true;
        dateFormat = "%r";
      };

      gitsigns.enable = true;

      indent-blankline.enable = true;

      lualine.enable = true;

      luasnip = {
        enable = true;
      };

      lsp = {
        enable = true;
        servers = {
          # bashls.enable = true;
          lua-ls.enable = true;
          nil_ls.enable = true;
          # rnix-lsp.enable = true;
          pyright.enable = true;
        };
        keymaps.diagnostic = {
          "<leader>j" = "goto_next";
          "<leader>k" = "goto_prev";
        };
        keymaps.lspBuf = {
          K = "hover";
          gD = "references";
          gd = "definition";
          gi = "implementation";
          gt = "type_definition";
        };
      };

      lsp-format = {
        enable = true;
        # lspServersToEnable = ["bashls" "lua-ls" "pyright"];
        lspServersToEnable = ["lua-ls" "pyright"];
      };

      markdown-preview.enable = true;

      noice.enable = true;

      notify.enable = true;

      nvim-cmp = {
        enable = true;
        autoEnableSources = true;
        sources = [
          {name = "copilot";}
          {name = "nvim_lsp";}
          {name = "luasnip";}
          {name = "path";}
          {name = "buffer";}
        ];
        snippet.expand = "luasnip";
        mapping = {
          "<C-e>" = "cmp.mapping.close()";
          "<CR>" = "cmp.mapping.confirm({ select = true })";
          "<Tab>" = {
            action = "cmp.mapping.select_next_item()";
            modes = ["i" "s"];
          };
        };
      };

      surround.enable = true;

      treesitter.enable = true;

      oil.enable = true;

      project-nvim.enable = true;

      nix.enable = true;

      which-key.enable = true;
    };
    extraPlugins = with pkgs.vimPlugins; [
      {plugin = helm_ls;}
      {
        plugin = glow;
        config = ''lua require("glow").setup()'';
      }
    ];
  };

  nixpkgs = {
    overlays = [
      (final: prev: {
        vimPlugins =
          prev.vimPlugins
          // {
            helm_ls = prev.vimUtils.buildVimPlugin {
              name = "helm_ls";
              src = inputs.nvim-plugin-helm_ls;
            };
            glow = prev.vimUtils.buildVimPlugin {
              name = "glow";
              src = inputs.nvim-plugin-glow;
            };
          };
      })
    ];
  };
}
