{
  imports = [
    ./apps/common.nix
    ./apps/starship.nix
    ./apps/zsh.nix
  ];

  home = {
    stateVersion = "23.11";
  };

  programs = {
    home-manager.enable = true;
    zellij.enable = true;
    zsh.initExtra = ''
      eval "$(/opt/homebrew/bin/brew shellenv)"
    '';
  };
}
