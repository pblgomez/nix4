{
  pkgs,
  unstablePkgs,
  ...
}:
with pkgs; [
  ## unstable
  unstablePkgs.yt-dlp
  unstablePkgs.htop
  unstablePkgs.kubectl-neat
  unstablePkgs.kubectl-view-secret

  ## stable
  jq
  neovim
  slack
  unzip
  xh

  # requires nixpkgs.config.allowUnfree = true;
  # vscode-extensions.ms-vscode-remote.remote-ssh

  # lib.optionals boolean stdenv is darwin
  #mas # mac app store cli
]
