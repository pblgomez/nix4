{
  pkgs,
  lib,
  inputs,
  ...
}: let
  inherit (inputs) nixpkgs;
  user = "pbl";
in {
  # imports = [../../kmonad/kmonad-darwin.nix];

  users.users.${user}.home = "/Users/${user}";
  age.identityPaths = ["/Users/${user}/.ssh/id_ed25519"];
  age.secrets = {
    awsCreds = {
      file = ../../secrets/awsCreds.age;
      path = "/Users/${user}/.config/aws/credentials";
      mode = "400";
      owner = "${user}";
      group = "staff";
    };
    bookmarks = {
      file = ../../secrets/bookmarks.age;
      path = "/Users/${user}/.local/share/bookmarks";
      mode = "400";
      owner = "${user}";
      group = "staff";
    };
    sshConfig = {
      file = ../../secrets/sshConfig.age;
      path = "/Users/${user}/.ssh/config";
      mode = "400";
      owner = "${user}";
      group = "staff";
    };
  };

  fonts = {
    fontDir.enable = true;
    fonts = with pkgs; [
      (nerdfonts.override {fonts = ["FiraCode" "Iosevka"];})
    ];
  };
  homebrew = {
    enable = true;
    onActivation.upgrade = true;
    onActivation.cleanup = "zap";
    brews = [];
    taps = [];
    casks = ["bitwarden" "lulu" "firefox" "joplin" "telegram-desktop"];
    masApps = {
      # "Tailscale" = 1475387142;
      # "UTM" = 1538878817;
      # "Wireguard" = 1451685025;
    };
  };
  nix = {
    buildMachines = [
      {
        hostName = "nix-linux-dev";
        system = "aarch64-linux";
        protocol = "ssh-ng";
        supportedFeatures = ["nixos-test" "benchmark" "big-parallel"];
      }
    ];
    distributedBuilds = true;
    gc = {
      automatic = true;
      interval = {
        Weekday = 0;
        Hour = 0;
        Minute = 0;
      };
      options = "--delete-older-than 30d";
    };
    settings = {
      auto-optimise-store = true;
      experimental-features = "nix-command flakes";
      warn-dirty = false;
    };
  };
  nixpkgs.config = {
    allowUnfree = true;
    overlays = [
      (final: prev:
        lib.optionalAttrs (prev.stdenv.system == "aarch64-darwin") {
          # Add access to x86 packages system is running Apple Silicon
          pkgs-x86 = import nixpkgs {
            system = "x86_64-darwin";
            config.allowUnfree = true;
          };
        })
    ];
  };

  programs.zsh = {
    enable = true;
    enableCompletion = true;
  };
  environment.etc."zshenv.local".text = ''
    export ZDOTDIR="$HOME/.config/zsh"
  '';

  security.pam.enableSudoTouchIdAuth = true;
  services.nix-daemon.enable = true;

  # macOS configuration
  system = {
    activationScripts.postUserActivation.text = ''
      # Following line should allow us to avoid a logout/login cycle
      /System/Library/PrivateFrameworks/SystemAdministration.framework/Resources/activateSettings -u
    '';
    defaults = {
      dock.mru-spaces = false;
      NSGlobalDomain = {
        AppleShowAllExtensions = true;

        # NSGlobalDomain.AppleShowScrollBars = "Always";
        # NSGlobalDomain.NSUseAnimatedFocusRing = false;
        # NSGlobalDomain.NSNavPanelExpandedStateForSaveMode = true;
        # NSGlobalDomain.NSNavPanelExpandedStateForSaveMode2 = true;
        # NSGlobalDomain.PMPrintingExpandedStateForPrint = true;
        # NSGlobalDomain.PMPrintingExpandedStateForPrint2 = true;
        # NSGlobalDomain.NSDocumentSaveNewDocumentsToCloud = false;
        ApplePressAndHoldEnabled = false;
        # NSGlobalDomain.InitialKeyRepeat = 25;
        # NSGlobalDomain.KeyRepeat = 4;
        "com.apple.mouse.tapBehavior" = 1;
      };
      LaunchServices.LSQuarantine = false; # disables "Are you sure?" for new apps
      # loginwindow.GuestEnabled = false;
    };
    defaults.CustomUserPreferences = {
      "com.apple.finder" = {
        ShowExternalHardDrivesOnDesktop = true;
        ShowHardDrivesOnDesktop = false;
        ShowMountedServersOnDesktop = false;
        ShowRemovableMediaOnDesktop = true;
        _FXSortFoldersFirst = true;
        # When performing a search, search the current folder by default
        FXDefaultSearchScope = "SCcf";
        DisableAllAnimations = true;
        # NewWindowTarget = "PfDe";
        # NewWindowTargetPath = "file://$\{HOME\}/Desktop/";
        AppleShowAllExtensions = true;
        FXEnableExtensionChangeWarning = false;
        ShowStatusBar = true;
        ShowPathbar = true;
        WarnOnEmptyTrash = false;
      };
      "com.apple.desktopservices" = {
        # Avoid creating .DS_Store files on network or USB volumes
        DSDontWriteNetworkStores = true;
        DSDontWriteUSBStores = true;
      };
      "com.apple.dock" = {
        autohide = true;
        launchanim = false;
        static-only = false;
        show-recents = false;
        show-process-indicators = true;
        orientation = "left";
        tilesize = 36;
        minimize-to-application = true;
        mineffect = "scale";
      };
      "com.apple.ActivityMonitor" = {
        OpenMainWindow = true;
        IconType = 5;
        SortColumn = "CPUUsage";
        SortDirection = 0;
      };
      # "com.apple.Safari" = {
      #   # Privacy: don’t send search queries to Apple
      #   UniversalSearchEnabled = false;
      #   SuppressSearchSuggestions = true;
      # };
      "com.apple.AdLib" = {
        allowApplePersonalizedAdvertising = false;
      };
      "com.apple.SoftwareUpdate" = {
        AutomaticCheckEnabled = true;
        # Check for software updates daily, not just once per week
        ScheduleFrequency = 1;
        # Download newly available updates in background
        AutomaticDownload = 1;
        # Install System data files & security updates
        CriticalUpdateInstall = 1;
      };
      "com.apple.TimeMachine".DoNotOfferNewDisksForBackup = true;
      # Prevent Photos from opening automatically when devices are plugged in
      "com.apple.ImageCapture".disableHotPlug = true;
      # Turn on app auto-update
      "com.apple.commerce".AutoUpdate = true;
      "com.googlecode.iterm2".PromptOnQuit = false;
      "com.google.Chrome" = {
        #AppleEnableSwipeNavigateWithScrolls = false;
        DisablePrintPreview = true;
        PMPrintingExpandedStateForPrint2 = true;
      };
    };
  };
}
