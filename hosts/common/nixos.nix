{
  pkgs,
  lib,
  ...
}: let
  username = "pbl";
in {
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.allowUnfreePredicate = pkg:
    builtins.elem (lib.getName pkg) [
      "terraform"
    ];
  age.secrets = {
    age = {
      file = ../../secrets/age.age;
      path = "/home/${username}/.config/sops/age/keys.txt";
      mode = "400";
      owner = username;
      group = "users";
    };
    awsCreds = {
      file = ../../secrets/awsCreds.age;
      path = "/home/${username}/.config/aws/credentials";
      mode = "400";
      owner = username;
      group = "users";
    };
    bookmarks = {
      file = ../../secrets/bookmarks.age;
      path = "/home/${username}/.local/share/bookmarks";
      mode = "400";
      owner = username;
      group = "users";
    };
    sshConfig = {
      file = ../../secrets/sshConfig.age;
      path = "/home/${username}/.ssh/config";
      mode = "400";
      owner = username;
      group = "users";
    };
    email = {
      file = ../../secrets/email.age;
      path = "/home/${username}/.cache/agenix/email";
      mode = "400";
      owner = username;
      group = "users";
    };
    email-veset = {
      file = ../../secrets/email-veset.age;
      path = "/home/${username}/.cache/agenix/email-veset";
      mode = "400";
      owner = username;
      group = "users";
    };
    bwAddress = {
      file = ../../secrets/bwAddress.age;
      path = "/home/${username}/.cache/agenix/bwAddress";
      mode = "400";
      owner = username;
      group = "users";
    };
  };
  fonts.packages = with pkgs; [(nerdfonts.override {fonts = ["FiraCode" "Iosevka"];})];
  environment.systemPackages = with pkgs; [
    brightnessctl

    telegram-desktop
    terraform
    sxiv
    bitwarden
    swayidle
    swaylock-effects
    wl-clipboard
    xdg-utils
  ];

  programs.zsh.enable = true;
  programs.hyprland.enable = true; # fos hyprland deps
  environment.sessionVariables = {
    XDG_CONFIG_HOME = "$HOME/.config";
  };
  environment.etc."zshenv.local".text = ''
    export ZDOTDIR="$HOME/.config/zsh"
  '';
  security.pam.services.swaylock.text = ''
    # PAM configuration file for the swaylock screen locker. By default, it includes
    # the 'login' configuration file (see /etc/pam.d/login)
    auth include login
  '';

  services.kanata = {
    enable = true;
    package = pkgs.kanata-with-cmd;
    keyboards.infinity.devices = ["/dev/input/by-path/platform-i8042-serio-0-event-kbd"];
    keyboards.infinity.extraDefCfg = ''
      process-unmapped-keys yes
      linux-unicode-u-code v
    '';
    keyboards.infinity.config = ''
      #|
      (defsrc
        esc       f1   f2   f3  f4 f5 f6   f7   f8   f9   f10  f11  f12  prnt pause ins del
        grv       1    2    3   4  5  6    7    8    9    0    -    =    bspc
        tab       q    w    e   r  t  y    u    i    o    p    [    ]    ret
        caps      a    s    d   f  g  h    j    k    l    ;    '    \
        lsft 102d z    x    c   v  b  n    m    ,    .    /    rsft
        lctl      lmet lalt spc                 ralt rctl pgup up   pgdn
                                                          left down rght
      )
      |#

      (defvar
        tap-timeout    100
        hold-timeout   200
        tt $tap-timeout
        ht $hold-timeout

        mouse-interval 3
        mouse-distance 1
        mouse-distance-minimum 1
        mouse-distance-maximum 4
        mi $mouse-interval
        mdm $mouse-distance-minimum
        mdM $mouse-distance-maximum
      )

      (defsrc
                  q       w     e     r     y     u     i     o     p
        caps      a       s     d     f     h     j     k     l     ;
                  z       x     c     v     n     m     ,     .     /
                          lalt  spc                     ralt
      )

      (deflayer default
                  q       @escw @esce r     y     u     @bspi @bspo p
        esc       @meta   @alts @ctld @sftf h     @sftj @ctlk @altl @met;
                  z       x     c     v     n     m     ,     .     /
                          @numt @spcm                   @navr
      )

      (deflayer nav
                  XX      XX    RA-e  XX    XX    RA-u  RA-i  RA-o  XX
        XX        RA-a    XX    XX    sft   lft   down  up    rght  XX
                  brdown  brup  C-S-c C-S-v RA-n  mute  vold  volu  XX
                          XX    XX                      XX
      )

      (deflayer num
                  `       '     S-9   S-0   \     7     8     9     -
        XX        met     alt   ctl   sft   XX    4     5     6     +
                  S-[     S-]   [     ]     0     1     2     3     =
                          XX    XX                      XX
      )

      (deflayer escw
                  XX      XX    esc   XX    XX    XX    XX    bspc  XX
        XX        XX      XX    XX    XX    XX    XX    XX    XX    XX
                  XX      XX    XX    XX    XX    XX    XX    XX    XX
                          XX    XX                      XX
      )

      (deflayer esce
                  XX      esc   XX    XX    XX    XX    XX    XX    XX
        XX        XX      XX    XX    XX    XX    XX    XX    XX    XX
                  XX      XX    XX    XX    XX    XX    XX    XX    XX
                          XX    XX                      XX
      )

      (deflayer bspi
                  XX      XX    XX    XX    XX    XX    XX    bspc  XX
        XX        XX      XX    XX    XX    XX    XX    XX    XX    XX
                  XX      XX    XX    XX    XX    XX    XX    XX    XX
                          XX    XX                      XX
      )

      (deflayer bspo
                  XX      XX    XX    XX    XX    XX    bspc  XX    XX
        XX        XX      XX    XX    XX    XX    XX    XX    XX    XX
                  XX      XX    XX    XX    XX    XX    XX    XX    XX
                          XX    XX                      XX
      )

      (deflayer mouse
                  XX      XX    XX    XX    @mwl  @mwd  @mwu  @mwr  XX
        XX        XX      XX    XX    XX    @mlf  @mdw  @mup  @mrg  XX
                  XX      XX    XX    XX    XX    mlft  mmid  mrgt  XX
                          XX    XX                      XX
      )

      ;; (deflayer qwerty
      ;;   esc       f1   f2   f3  f4 f5 f6   f7   f8   f9   f10  f11  f12  prnt pause ins del
      ;;   grv       1    2    3   4  5  6    7    8    9    0    -    =    bspc
      ;;   tab       q    w    e   r  t  y    u    i    o    p    [    ]    ret
      ;;   caps      a    s    d   f  g  h    j    k    l    ;    '    \
      ;;   lsft 102d z    x    c   v  b  n    m    ,    .    /    rsft
      ;;   lctl      lmet lalt spc                 ralt rctl pgup up   pgdn
      ;;                                                     left down rght
      ;; )

      (defalias  ;; layers
        spcm (tap-hold $tt $ht spc (layer-while-held mouse))
        navr (tap-hold $tt $ht ret (layer-while-held nav))
        numt (tap-hold $tt $ht tab (layer-while-held num))

        bspi (tap-hold-press 10 $ht i (layer-while-held bspi))
        bspo (tap-hold-press 10 $ht o (layer-while-held bspo))
        escw (tap-hold-press 10 $ht w (layer-while-held escw))
        esce (tap-hold-press 10 $ht e (layer-while-held esce))
      )

      (defalias  ;; home-row mod
        meta (tap-hold $tt $ht a met)
        alts (tap-hold $tt $ht s alt)
        ctld (tap-hold $tt $ht d ctl)
        sftf (tap-hold $tt $ht f sft)
        sftj (tap-hold-release-timeout $tt $ht j sft j)
        ctlk (tap-hold-release-timeout $tt $ht k ctl k)
        altl (tap-hold-release-timeout $tt $ht l alt l)
        met; (tap-hold $tt $ht ; met)
      )

      (defalias  ;; mouse
        mlf (movemouse-accel-left  $mi 1000 $mdm $mdM)
        mdw (movemouse-accel-down  $mi 1000 $mdm $mdM)
        mup (movemouse-accel-up    $mi 1000 $mdm $mdM)
        mrg (movemouse-accel-right $mi 1000 $mdm $mdM)
        mwl (mwheel-left $mi $mdM)
        mwd (mwheel-down $mi $mdM)
        mwu (mwheel-up $mi $mdM)
        mwr (mwheel-right $mi $mdM)
      )
    '';
  };
  virtualisation = {
    podman = {
      enable = true;
      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = true;
      # Required for containers under podman-compose to be able to talk to each other.
      defaultNetwork.settings.dns_enabled = true;
    };
    libvirtd.enable = true;
  };
  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = ["user-with-access-to-virtualbox"];
  programs.virt-manager.enable = true;
  users.users.${username}.extraGroups = ["libvirtd"];
}
