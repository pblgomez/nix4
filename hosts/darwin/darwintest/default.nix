{
  pkgs,
  unstablePkgs,
  ...
}: {
  environment.systemPackages =
    import ./../../common/common-packages.nix
    {
      inherit pkgs;
      inherit unstablePkgs;
    };
}
